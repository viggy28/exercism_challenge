// This is a "stub" file.  It's a little start on your solution.
// It's not a complete solution though; you have to write some code.

// Package acronym should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package acronym

import "strings"

// Abbreviate should have a comment documenting it.
func Abbreviate(s string) string {
	s = strings.Replace(s, "-", " ", -1)
	ss := strings.Split(s, " ")
	acr := ""
	for _, word := range ss {
		if word == "-" {
			continue
		}
		acr = acr + strings.ToUpper(word[:1])
	}
	return acr
}
