package space

// Age function returns the age based on the sec in the planet
func Age(sec float64, planet string) float64 {
	if planet == "Earth" {
		return sec / 31557600
	} else if planet == "Mercury" {
		return sec / 31557600 / 0.2408467
	} else if planet == "Venus" {
		return sec / 31557600 / 0.61519726
	} else if planet == "Mars" {
		return sec / 31557600 / 1.8808158
	} else if planet == "Jupiter" {
		return sec / 31557600 / 11.862615
	} else if planet == "Saturn" {
		return sec / 31557600 / 29.447498
	} else if planet == "Uranus" {
		return sec / 31557600 / 84.016846
	} else if planet == "Neptune" {
		return sec / 31557600 / 164.79132
	}
	return 0.0
}
