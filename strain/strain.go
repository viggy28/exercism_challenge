package strain

type Ints []int
type Lists [][]int
type Strings []string

func (i Ints) Keep(fn func(int) bool) (o Ints) {
	for _, n := range i {
		if fn(n) {
			o = append(o, n)
		}
	}
	return o
}

func (i Ints) Discard(fn func(int) bool) (o Ints) {
	for _, n := range i {
		if !fn(n) {
			o = append(o, n)
		}
	}
	return o
}

func (i Strings) Keep(fn func(string) bool) (o Strings) {
	for _, word := range i {
		if fn(word) {
			o = append(o, word)
		}
	}
	return o
}

func (l Lists) Keep(fn func([]int) bool) (o Lists) {
	for _, list := range l {
		if fn(list) {
			o = append(o, list)
		}
	}
	return o
}
