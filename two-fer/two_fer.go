// Package twofer is short for two for one. One for you and one for me.
package twofer

// ShareWith returns who I am go to share
func ShareWith(name string) string {
	v1 := "One for "
	v2 := ", one for me."
	if name != "" {
		return v1 + name + v2
	}
	return v1 + "you" + v2
}
