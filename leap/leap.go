// Package leap helps to find whether a year is leap year or not
package leap

// IsLeapYear returns whether a year is leap year or not
func IsLeapYear(n int) bool {
	if n%4 == 0 {
		if n%100 == 0 {
			if n%400 == 0 {
				return true
			}
			return false
		}
		return true
	}
	return false
}
