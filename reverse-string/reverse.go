package reverse

// String reverses a given string & returns
func String(ip string) (op string) {
	ipRune := []rune(ip)
	for i := len(ipRune) - 1; i >= 0; i-- {
		op += string(ipRune[i])
	}
	return op
}
