package isogram

import "strings"

// IsIsogram return whether a string is isogram or not. Implementation using map
func IsIsogram(ip string) bool {
	ip = strings.ToLower(ip)
	exists := make(map[rune]bool)
	for _, char := range ip {
		if char == ' ' || char == '-' {
			continue
		}
		if _, ok := exists[char]; ok {
			return false
		}
		exists[char] = true
	}
	return true
}

// IsIsogram return whether a string is isogram or not. Implementation using for loop
/* func IsIsogram(ip string) bool {
	ip = strings.ToLower(ip)
	ipRune := []rune(ip)
	for index := 0; index < len(ip); index++ {
		char := ipRune[index]
		if char == ' ' || char == '-' {
			continue
		}
		for index1 := index + 1; index1 < len(ip); index1++ {
			char1 := ipRune[index1]
			if char == char1 {
				return false
			}
		}
	}
	return true
} */
