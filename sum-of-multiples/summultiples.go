package summultiples

// SumMultiles gives the multiples
func SumMultiples(limit int, divisors []int) int {
	var sum int
	exists := make(map[int]bool)
	for j := 0; j < len(divisors); j++ {
		for i := 0; i < limit; i++ {
			if divisors[j] == 0 {
				continue
			}
			if i%divisors[j] == 0 {
				if !exists[i] {
					sum += i
					exists[i] = true
				}
			}
		}
	}
	return sum
}
