package luhn

import (
	"strconv"
)

// Valid checks whether a given string is luhn valid or not
func Valid(ip string) bool {
	var sum int
	var inp []int
	//converting string to int slice
	for _, char := range ip {
		if string(char) == " " {
			continue
		}
		n, err := strconv.Atoi(string(char))
		if err == nil || err.Error() == "strconv.Atoi: parsing \" \": invalid syntax" {
			inp = append(inp, n)
		} else {
			return false
		}
	}

	if len(inp) < 2 {
		// not a valid luhn number
		return false
	}
	// check whether the inp length is odd number
	for index, n := range inp {
		if len(inp)%2 == 1 && index%2 == 1 {
			calcumateSum(&sum, n)
			continue
		}
		if len(inp)%2 == 0 && index%2 == 0 {
			calcumateSum(&sum, n)
			continue
		}
		sum += n
	}
	if sum%10 != 0 {
		// not a valid luhn number
		return false
	}
	return true
}

func calcumateSum(sum *int, n int) {
	n *= 2
	if n > 9 {
		n = n - 9
	}
	*sum += n
}
