// Package hamming have functions to find the hamming distance
package hamming

import "errors"

// Distance returns the hamming distance of the two strings
func Distance(a, b string) (int, error) {
	if len(a) != len(b) {
		return 0, errors.New("strings are of different length")
	}
	var distance int
	runeA := []rune(a)
	runeB := []rune(b)
	for i := 0; i < len(a); i++ {
		if runeA[i] != runeB[i] {
			distance++
		}
	}
	return distance, nil
}
