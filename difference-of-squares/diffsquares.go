package diffsquares

import "math"

// SquareOfSum returns Square of sum of the first n natural numbers implemented using pow()
func SquareOfSum(n int) (op int) {
	op = int(math.Pow(float64(n*(n+1.0)/2.0), 2.0))
	return op
}

/* // SumOfSquares returns sum of Square of the first n natural numbers
func SumOfSquares(n int) int {
	var sum int
	for index := 1; index <= n; index++ {
		sum += (index) * (index)
	}
	return sum
} */

// SumOfSquares returns sum of Square of the first n natural numbers implemented using formula
func SumOfSquares(n int) int {
	op := n * (n + 1) * (2*n + 1) / 6
	return op
}

/* // SumOfSquares returns sum of Square of the first n natural numbers
func SumOfSquares(n int) int {
	var sum int
	for index := 1; index <= n; index++ {
		sum += (index) * (index)
	}
	return sum
} */

// Difference returns the difference of SquareOfSum & SumOfSquares
func Difference(n int) int {
	return SquareOfSum(n) - SumOfSquares(n)
}
