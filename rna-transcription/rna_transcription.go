package strand

func ToRNA(dna string) string {
	dnatorna := make(map[string]string)
	dnatorna["C"] = "G"
	dnatorna["G"] = "C"
	dnatorna["T"] = "A"
	dnatorna["A"] = "U"
	dnatorna[""] = ""
	if len(dna) == 1 {
		rna := dnatorna[dna]
		return rna
	}
	var output string
	for _, charDna := range dna {
		output += dnatorna[string(charDna)]
	}
	return output
}
